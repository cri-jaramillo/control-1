package cl.duoc.ejerciciologin.Entidades;

/**
 * Created by DUOC on 24-03-2017.
 */

public class Persona {
    private String usuario,clave,nombre,apellido,rut,fecha;


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getRut() {
        return rut;
    }

    public void setRut(String rut) {
        this.rut = rut;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Persona() {

    }


    public Persona(String usuario, String clave, String nombre, String apellido, String rut, String fecha) {
        setUsuario(usuario);
        setClave(clave);
        setNombre(nombre);
        setApellido(apellido);
        setRut(rut);
        setFecha(fecha);
    }
}
