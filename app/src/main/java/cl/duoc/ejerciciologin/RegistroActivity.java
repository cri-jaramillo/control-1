package cl.duoc.ejerciciologin;

import android.app.DatePickerDialog;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.ejerciciologin.Entidades.ListaPersona;
import cl.duoc.ejerciciologin.Entidades.Persona;


/**
 * Created by DUOC on 24-03-2017.
 */

public class RegistroActivity extends AppCompatActivity {

    EditText  nombre,apellido,usuario,rut,clave,claveR;
    Button fecha, registro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);




        nombre = (EditText)findViewById(R.id.etNombre);
        apellido = (EditText)findViewById(R.id.etApellido);
        usuario = (EditText)findViewById(R.id.etUsuarioR);
        clave = (EditText)findViewById(R.id.etClaveR);
        rut = (EditText)findViewById(R.id.etRut);
        claveR =(EditText)findViewById(R.id.etRepetirClaveR);
        fecha = (Button)findViewById(R.id.etFecha);
        registro = (Button) findViewById(R.id.btnRegistroR);
        fecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().show();
            }
        });
        registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GuardarPersona();


            }
        });






    }

    private void GuardarPersona()
    {
        String mensajeError ="";
        ListaPersona l1 = new ListaPersona();
        Persona p1 = new Persona();


        if(nombre.getText().toString().length()<1){
            mensajeError += "ingrese un nombre valido";
        }
        if(apellido.getText().toString().length()<1){
            mensajeError +="ingrese un apellido valido";

        }
        if(usuario.getText().toString().length()<1){

            mensajeError +="ingrese un usuario valido";
            l1.validarUsuario(usuario.getText().toString());


        }
        if(!isValidarRut(rut.getText().toString())){
            mensajeError +="ingrese un rut valido";
        }
        if(!clave.getText().toString().equals(claveR.getText().toString())){

            mensajeError +="las claves no coinciden";
        }
        if(nombre.getText().toString().length()>1){

            p1.setUsuario(usuario.getText().toString());
            p1.setNombre(nombre.getText().toString());
            p1.setApellido(apellido.getText().toString());
            p1.setClave(clave.getText().toString());
            p1.setRut(rut.getText().toString());
            p1.setFecha(fecha.getText().toString());

            l1.Agregar(p1);
            Toast.makeText(this, "Datos Guardados Correctamente", Toast.LENGTH_LONG).show();

        }
        else {
            Toast.makeText(this, mensajeError, Toast.LENGTH_LONG).show();
        }
    }

    public boolean isValidarRut(String rut) {

        boolean validacion = false;
        try {
            rut = rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (java.lang.NumberFormatException e) {
        } catch (Exception e) {
        }
        return validacion;
    }



    private DatePickerDialog getDialog() {
        Calendar c = Calendar.getInstance();
        int anio = c.get(Calendar.YEAR);
        int mes = c.get(Calendar.MONTH);
        int dia = c.get(Calendar.DAY_OF_MONTH);


        return new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                fecha.setText(year + "/" + formatDate((month + 1)) + "/" + formatDate(dayOfMonth));
            }
        }, anio, mes, dia);

    }

    private String formatDate(int value) {

        return (value > 9 ? value + "" : "0" + value);

    }

    }





