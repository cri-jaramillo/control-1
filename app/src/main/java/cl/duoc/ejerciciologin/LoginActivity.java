package cl.duoc.ejerciciologin;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import cl.duoc.ejerciciologin.Entidades.ListaPersona;

public class LoginActivity extends AppCompatActivity {
    ListaPersona l1 = new ListaPersona();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        Button btnEntrar = (Button) findViewById(R.id.btnEntrar);
        Button btnRegistro = (Button)findViewById(R.id.btnRegistro);

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EditText etUsuario = (EditText) findViewById(R.id.etUsuario);
                EditText etContra = (EditText) findViewById(R.id.etContra);
                String usuario = etUsuario.getText().toString();
                String contra = etContra.getText().toString();



                Toast.makeText(LoginActivity.this, l1.Login(usuario,contra).toString() , Toast.LENGTH_LONG).show();
                Intent i = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(i);
            }
        }
        );

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(LoginActivity.this, RegistroActivity.class);
                startActivity(i);
            }
        });

    }


}
