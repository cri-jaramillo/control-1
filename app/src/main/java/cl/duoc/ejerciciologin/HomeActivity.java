package cl.duoc.ejerciciologin;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import cl.duoc.ejerciciologin.Entidades.ListaPersona;

/**
 * Created by DUOC on 24-03-2017.
 */

public class HomeActivity extends AppCompatActivity {
    ListaPersona l1 = new ListaPersona();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        Button listar = (Button)findViewById(R.id.btnListar);


        listar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomeActivity.this, l1.Listar().toString(), Toast.LENGTH_SHORT).show();
            }
        });




    }

}
